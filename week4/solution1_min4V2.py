def min4(a, b, c, d):
    m1 = min(a, b)
    m2 = min(c, d)
    return min(m1, m2)


a, b, c, d = int(input()), int(input()), int(input()), int(input())
print(min4(a, b, c, d))
