import math


def length(a1, b1, a2, b2):
    return math.hypot(a2 - a1, b2 - b1)


x1, y1 = float(input()), float(input())
x2, y2 = float(input()), float(input())
x3, y3 = float(input()), float(input())
l1 = length(x1, y1, x2, y2)
l2 = length(x1, y1, x3, y3)
l3 = length(x2, y2, x3, y3)
perimeter = l1 + l2 + l3
print(round(perimeter, 6))
