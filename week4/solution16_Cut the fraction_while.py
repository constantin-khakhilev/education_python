def reduceFraction(n, m):
    i = min(n, m)
    while i <= n:
        if n % i == 0 and m % i == 0:
            return print(n // i, m // i)
        i -= 1


n, m = int(input()), int(input())
print(reduceFraction(n, m))
