import math


def isPrime(n):
    i = 2
    while i <= n:
        if i > math.sqrt(n):
            return n == n
        if n % i == 0:
            return i == n
        i += 1


n = int(input())
if isPrime(n):
    print('YES')
else:
    print('NO')
