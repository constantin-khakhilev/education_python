import math


def isPointInCircle(x, y, xc, yc, r):
    return math.hypot(xc - x, yc - y) <= r


x, y = float(input()), float(input())
xc, yc = float(input()), float(input())
r = float(input())
if isPointInCircle(x, y, xc, yc, r):
    print('YES')
else:
    print('NO')
