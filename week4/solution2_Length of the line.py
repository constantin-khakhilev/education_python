import math


def distance(x1, y1, x2, y2):
    return math.hypot(x2 - x1, y2 - y1)


x1, y1 = float(input()), float(input())
x2, y2 = float(input()), float(input())
print(distance(x1, y1, x2, y2))
