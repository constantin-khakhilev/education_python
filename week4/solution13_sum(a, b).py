def sum(a, b):
    if b == 0:
        return a
    a = sum(a, b - 1) + 1
    return a


a, b = int(input()), int(input())
print(sum(a, b))
