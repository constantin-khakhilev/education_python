import math


def minDivisor(n):
    i = 2
    while i <= n:
        if i > math.sqrt(n):
            return n
        if n % i == 0:
            return i
        i += 1


n = int(input())
print(minDivisor(n))
