numList = list(map(int, input().split()))
morePrevious = list()
for i in range(1, len(numList)):
    if numList[i] > numList[i - 1]:
        morePrevious.append(numList[i])
print(' '.join(map(str, morePrevious)))
