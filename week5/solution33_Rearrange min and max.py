numList = list(map(int, input().split()))
iMin = numList.index(min(numList))
iMax = numList.index(max(numList))
numList[iMin], numList[iMax] = numList[iMax], numList[iMin]
print(*numList)
