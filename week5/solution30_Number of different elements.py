numList = list(map(int, input().split()))
numDif = 1
for i in range(1, len(numList)):
    if numList[i] != numList[i - 1]:
        numDif += 1
print(numDif)
