n = int(input())
numList = list(map(int, input().split()))
x = int(input())
minDifference = 2000
nearNum = numList[0]
for i in range(n):
    if minDifference > abs(numList[i] - x):
        minDifference = abs(numList[i] - x)
        nearNum = numList[i]
print(nearNum)
