numList = list(map(int, input().split()))
iMax = 0
nMax = 0
for i in range(0, len(numList)):
    if numList[i] >= nMax:
        nMax = numList[i]
        iMax = i
print(nMax, iMax)
