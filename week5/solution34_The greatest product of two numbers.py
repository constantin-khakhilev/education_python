numList = list(map(int, input().split()))
max1, max2 = 0, 0
min1, min2 = 0, 0
for i in range(len(numList)):
    if numList[i] >= max1:
        max2 = max1
        max1 = numList[i]
    elif numList[i] >= max2:
        max2 = numList[i]
    if numList[i] <= min1:
        min2 = min1
        min1 = numList[i]
    elif numList[i] <= min2:
        min2 = numList[i]
if max1 * max2 > min1 * min2:
    print(max2, max1)
else:
    print(min1, min2)
