numList = list(map(int, input().split()))
k = int(input())
for i in range(0, len(numList)):
    if i > k:
        numList[i - 1] = numList[i]
numList.pop()
print(' '.join(map(str, numList)))
