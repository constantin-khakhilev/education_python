numList = list(map(int, input().split()))
insIt = list(map(int, input().split()))
k = insIt[0]
c = insIt[1]
numList.append(0)
for i in range(len(numList)-1, k, -1):
    numList[i] = numList[i - 1]
numList[k] = c
print(' '.join(map(str, numList)))
