n = int(input())
previous = 0
nextF = 1
numFib = 0
i = 0
while i != n:
    numFib = previous + nextF
    nextF = previous
    previous = numFib
    i += 1
print(numFib)
