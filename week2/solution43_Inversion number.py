n = int(input())
invNum = 0
while n != 0:
    invNum = str(invNum) + str(n % 10)
    n //= 10
print(int(invNum))
