n = int(input())
i = 1
sumSquares = 0
while i <= n:
    sumSquares += i**2
    i += 1
print(sumSquares)
