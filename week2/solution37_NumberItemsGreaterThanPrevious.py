now = int(input())
previous = now
i = 0
while now != 0:
    if now > previous:
        i += 1
    previous = now
    now = int(input())
print(i)
