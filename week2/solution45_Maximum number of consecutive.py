now = int(input())
previous = now
i = 0
iMax = i
while now != 0:
    if now == previous:
        i += 1
        if iMax < i:
            iMax = i
    else:
        if iMax < i:
            iMax = i
        i = 1
    previous = now
    now = int(input())
print(iMax)
