k = int(input())
nowNum = 1
n = 0
palNum = 0
while nowNum <= k:
    n = nowNum
    invNum = 0
    while n != 0:
        invNum = str(invNum) + str(n % 10)
        n //= 10
    if nowNum == int(invNum):
        palNum += 1
    nowNum += 1
print(palNum)
