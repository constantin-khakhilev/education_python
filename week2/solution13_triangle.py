a = int(input())
b = int(input())
c = int(input())
if a >= b and a >= c:
    (a, c) = (c, a)
if b >= a and b >= c:
    (b, c) = (c, b)
if a <= 0 or b <= 0 or c <= 0:
    print('impossible')
elif a + b <= c:
    print('impossible')
elif a**2 + b**2 == c**2:
    print('rectangular')
elif a**2 + b**2 < c**2:
    print('obtuse')
elif a**2 + b**2 > c**2:
    print('acute')
