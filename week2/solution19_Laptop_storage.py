a1, b1, c1 = int(input()), int(input()), int(input())
a2, b2, c2 = int(input()), int(input()), int(input())
k1 = (a1 // a2) * (b1 // b2) * (c1 // c2)
k2 = (a1 // a2) * (b1 // c2) * (c1 // b2)
k3 = (a1 // b2) * (b1 // a2) * (c1 // c2)
k4 = (a1 // b2) * (b1 // c2) * (c1 // a2)
k5 = (a1 // c2) * (b1 // a2) * (c1 // b2)
k6 = (a1 // c2) * (b1 // b2) * (c1 // a2)
maxBoxes = k1
if maxBoxes < k2:
    maxBoxes = k2
if maxBoxes < k3:
    maxBoxes = k3
if maxBoxes < k4:
    maxBoxes = k4
if maxBoxes < k5:
    maxBoxes = k5
if maxBoxes < k6:
    maxBoxes = k6
print(maxBoxes)
