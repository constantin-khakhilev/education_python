n = int(input())
if (n % 10) == 0 or 11 <= n <= 19 or 5 <= (n % 10) <= 9:
    print(n, 'korov')
elif (n % 10) == 1 and n != 11:
    print(n, 'korova')
elif 2 <= (n % 10) <= 4:
    print(n, 'korovy')
