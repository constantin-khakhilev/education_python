inFile = open('input.txt', 'r', encoding='utf8')
text = inFile.readlines()
words = list()
wordsDict = dict()
listDict = []
for line in text:
    tempData = list(str(line).split())
    words += tempData
for word in words:
    wordsDict[word] = wordsDict.get(word, 0) - 1
for word in wordsDict:
    listDict.append((wordsDict[word], word))
listDict.sort()
for i in range(len(listDict)):
    print(listDict[i][1])
