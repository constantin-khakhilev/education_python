import sys


text = sys.stdin.readlines()
words = set()
for line in text:
    tempData = set(str(line).split())
    words |= tempData
print(len(words))
