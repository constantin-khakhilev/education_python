inFile = open('input.txt', 'r', encoding='utf8')
text = inFile.readlines()
words = list()
wordsDict = dict()
for line in text:
    tempData = list(str(line).split())
    words += tempData
for word in words:
    wordsDict[word] = wordsDict.get(word, 0)
    print(wordsDict[word], end=' ')
    wordsDict[word] += 1
