n = int(input())
myDict = dict()
for i in range(n):
    line = str(input()).split()
    myDict[line[0]] = line[1]
synonym = input()
if synonym not in myDict:
    for words in myDict:
        if myDict[words] == synonym:
            print(words)
else:
    print(myDict[synonym])
