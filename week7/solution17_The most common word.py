inFile = open('input.txt', 'r', encoding='utf8')
text = inFile.readlines()
words = list()
wordsDict = dict()
mostWord = ''
nMost = 0
for line in text:
    tempData = list(str(line).split())
    words += tempData
for word in words:
    wordsDict[word] = wordsDict.get(word, 0) + 1
for word in sorted(wordsDict):
    if nMost < wordsDict[word]:
        nMost = wordsDict[word]
        mostWord = word
print(mostWord)
