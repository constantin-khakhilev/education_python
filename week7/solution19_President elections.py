inFile = open('input.txt', 'r', encoding='utf8')
outFile = open('output.txt', 'w', encoding='utf8')
text = inFile.readlines()
candidates = dict()
listCandidates = []
vote = 0
for line in text:
    tempData = (' '.join(line.split()))
    candidates[tempData] = candidates.get(tempData, 0) + 1
    vote += 1
for candidate in candidates:
    listCandidates.append((candidates[candidate], candidate))
listCandidates.sort()
if listCandidates[-1][0] > vote / 2:
    print(listCandidates[-1][1], file=outFile)
else:
    print(listCandidates[-1][1], file=outFile)
    print(listCandidates[-2][1], file=outFile)
inFile.close()
outFile.close()
