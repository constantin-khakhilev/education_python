n = int(input())
numbers = set(range(1, n + 1))
question = ''
yesSet = set()
noSet = set()
while question != 'HELP':
    if question == 'YES':
        prevQuestion = set(map(int, prevQuestion.split()))
        if yesSet == set():
            yesSet = prevQuestion
        yesSet &= prevQuestion
    if question == 'NO':
        prevQuestion = set(map(int, prevQuestion.split()))
        noSet |= prevQuestion
    prevQuestion = question
    question = input()
if yesSet == set():
    numbers -= noSet
else:
    yesSet -= noSet
    numbers &= yesSet
numbers = list(numbers)
numbers.sort()
print(*numbers)
