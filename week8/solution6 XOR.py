import operator


print(*map(lambda x: operator.xor(x[0], x[1]), zip(map(int, input().split()), map(int, input().split()))))
