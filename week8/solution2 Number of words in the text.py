import sys
import itertools


print(
    len(
        set(
            itertools.chain(
                str(
                    sys.stdin.read()
                ).split()
            )
        )
    )
)
