import math
k = float(input())
rem = k - int(k)
if rem < 0.5:
    print(math.floor(k))
else:
    print(math.ceil(k))
