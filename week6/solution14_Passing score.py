class Man:
    surname = ''
    name = ''
    score1 = 0
    score2 = 0
    score3 = 0


inFile = open('input.txt', 'r', encoding='utf8')
outFile = open('output.txt', 'w', encoding='utf8')
lines = inFile.readlines()
peopleList = []
for line in lines:
    tempManData = line.split()
    man = Man()
    man.surname = tempManData[0]
    man.name = tempManData[1]
    man.score1 = int(tempManData[2])
    man.score2 = int(tempManData[3])
    man.score3 = int(tempManData[4])
    if man.score1 >= 40 and man.score2 >= 40 and man.score3 >= 40:
        peopleList.append(man)
for man in peopleList:
    print(man.surname, man.name, man.score1, man.score2, man.score3, file=outFile)
inFile.close()
outFile.close()
