def countSort(myList):
    myList = list(map(int, myList.split()))
    grades = [0] * 101
    for now in myList:
        grades[now] += 1
    for grade in range(len(grades)):
        print((str(grade) + ' ') * grades[grade], end='')


inFile = open('input.txt', 'r', encoding='utf8')
nowList = inFile.read()
countSort(nowList)
inFile.close()
