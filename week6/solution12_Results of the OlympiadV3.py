inFile = open('input.txt', 'r', encoding='utf8')
outFile = open('output.txt', 'w', encoding='utf8')
n = inFile.readline()
lines = inFile.readlines()
peopleList = []
for line in lines:
    tempManData = line.split()
    peopleList.append((tempManData[0], int(tempManData[1])))
peopleList.sort(key=lambda peopleList: -peopleList[1])
for man in peopleList:
    print(man[0], file=outFile)
inFile.close()
outFile.close()
