class Man:
    surname = ''
    score = 0


inFile = open('input.txt', 'r', encoding='utf8')
outFile = open('output.txt', 'w', encoding='utf8')
n = inFile.readline()
lines = inFile.readlines()
peopleList = []
for line in lines:
    tempManData = line.split()
    man = Man()
    man.surname = tempManData[0]
    man.score = int(tempManData[1])
    peopleList.append(man)
peopleList.sort(key=lambda man: -man.score)
for man in peopleList:
    print(man.surname, file=outFile)
inFile.close()
outFile.close()
