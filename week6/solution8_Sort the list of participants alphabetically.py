class Man:
    surname = ''
    name = ''
    numSchool = 0
    score = 0


inFile = open('input.txt', 'r', encoding='utf8')
outFile = open('output.txt', 'w', encoding='utf8')
lines = inFile.readlines()
lines.sort()
peopleList = []
for line in lines:
    tempManData = line.split()
    man = Man()
    man.surname = tempManData[0]
    man.name = tempManData[1]
    man.numSchool = int(tempManData[2])
    man.score = int(tempManData[3])
    peopleList.append(man)
for man in peopleList:
    print(man.surname, man.name, man.score, file=outFile)
inFile.close()
outFile.close()
