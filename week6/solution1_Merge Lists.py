def merge(a, b):
    c = []
    i = 0
    j = 0
    while i < len(a) or j < len(b):
        if i < len(a) and j < len(b):
            if a[i] <= b[j]:
                c.append(a[i])
                i += 1
            else:
                c.append(b[j])
                j += 1
        elif i < len(a):
            c.append(a[i])
            i += 1
        elif j < len(b):
            c.append(b[j])
            j += 1
    print(*c)


a = list(map(int, input().split()))
b = list(map(int, input().split()))
merge(a, b)
