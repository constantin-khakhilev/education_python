n = int(input())
rngV = list(map(int, input().split()))
m = int(input())
rngS = list(map(int, input().split()))
for i in range(n):
    rngV[i] = (rngV[i], i + 1)
for i in range(m):
    rngS[i] = (rngS[i], i + 1)
rngV.sort()
rngS.sort()
planList = []
i = 0
j = 0
while i < n and j < m:
    if j + 1 >= m:
        planList.append((rngV[i][1], rngS[j][1]))
        i += 1
    elif abs(rngV[i][0] - rngS[j + 1][0]) < abs(rngV[i][0] - rngS[j][0]):
        j += 1
    else:
        planList.append((rngV[i][1], rngS[j][1]))
        i += 1
planList.sort()
answer = []
for i in range(n):
    answer.append(planList[i][1])
print(*answer)
