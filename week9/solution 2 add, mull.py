from sys import stdin
from copy import deepcopy


class Matrix(object):
    def __init__(self, matrix):
        self.matrix = deepcopy(matrix)

    def __str__(self):
        return '\n'.join('\t'.join(map(str, row)) for row in self.matrix)

    def __add__(self, other):
        newmatrix = deepcopy(self.matrix)
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                newmatrix[i][j] = self.matrix[i][j] + other.matrix[i][j]
        return Matrix(newmatrix)

    def __mul__(self, other):
        newmatrix = deepcopy(self.matrix)
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                newmatrix[i][j] = self.matrix[i][j] * other
        return Matrix(newmatrix)
    __rmul__ = __mul__

    def size(self):
        return len(self.matrix), len(self.matrix[0])


exec(stdin.read())
