from sys import stdin
from copy import deepcopy


class MatrixError(BaseException):
    def __init__(self, matrix, other):
        self.matrix1 = matrix
        self.matrix2 = other


class Matrix(object):
    def __init__(self, matrix):
        self.matrix = deepcopy(matrix)

    def size(self):
        return len(self.matrix), len(self.matrix[0])

    def __str__(self):
        return '\n'.join('\t'.join(map(str, row)) for row in self.matrix)

    def __add__(self, other):
        if len(self.matrix) == len(other.matrix) \
                and len(self.matrix[0]) == len(other.matrix[0]):
            newmatrix = deepcopy(self.matrix)
            for i in range(len(self.matrix)):
                for j in range(len(self.matrix[i])):
                    newmatrix[i][j] = self.matrix[i][j] + other.matrix[i][j]
        else:
            error = MatrixError(self, other)
            raise error
        return Matrix(newmatrix)

    def __mul__(self, other):
        newmatrix = deepcopy(self.matrix)
        for i in range(len(self.matrix)):
            for j in range(len(self.matrix[i])):
                newmatrix[i][j] = self.matrix[i][j] * other
        return Matrix(newmatrix)
    __rmul__ = __mul__

    def transpose(self):
        t_matrix = list(zip(*self.matrix))
        self.matrix = t_matrix
        return Matrix(t_matrix)

    def transposed(self):
        t_matrix = list(zip(*self.matrix))
        return Matrix(t_matrix)


exec(stdin.read())
